#!/usr/bin/env bash

##
# Search/replace the database for URL strings and such, so that
# WordPress knows what its own URL is, and that hard-coded links
# in content don't break out to the Production site.
##


# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi


# Work from the web root
cd /vagrant


# Set some vars from the environment
find_url=$PROD_URL
find_host=${find_url#*//}; find_host=${find_host%/}
set_url=$LOCAL_URL
set_host=${set_url#*//}; set_host=${set_host%/}

# Set "--url" option based on what's currently in the database.
if $(wp option get siteurl --url=$set_url --allow-root &> /dev/null); then
	prime_url=$set_url
else
	prime_url=$find_url
fi

# Run one or more search-replace jobs on the site, depending on
# whether or not this is a single or multisite instance.
if $(wp core is-installed --allow-root --network --url=$prime_url); then

	echo "Setting URLs across the network..."
	wp search-replace "$find_url" "$set_url" \
		--url=$prime_url \
		--all-tables \
		--allow-root \
		| grep "Success"
	wp search-replace "$find_host" "$set_host" \
		--url=$prime_url \
		--all-tables \
		--allow-root \
		| grep "Success"
	echo

	echo "Updating URLs for each site in the network..."; echo

	for url in $(wp site list --field=url --url=$prime_url --allow-root); do
		echo "Setting site URL options for '${url[@]}'..."

		# Set a var that we can run substring operations on.
		this_url=${url[@]}

		# Strip protocol prefixes & trailing slash.
		url_stripped=${this_url#*//}
		url_stripped=${url_stripped%/}

		# Set the site's URL-related options
		wp option set home http://$url_stripped --url=$this_url --allow-root
		wp option set siteurl http://$url_stripped --url=$this_url --allow-root

		# Set consisten URL prefix for site's 1st-party URLs
		wp search-replace "https://$url_stripped" "http://$url_stripped" \
			--url=$this_url \
			--allow-root \
			| grep "Success"
		echo

	done
else
	echo "Replacing '$find_url' with '$set_url'..."
	wp search-replace "$find_url" "$set_url" \
		--skip-columns=guid \
		--allow-root \
		| grep "Success"
fi

