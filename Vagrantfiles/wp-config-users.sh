#!/usr/bin/env bash

# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi

cd /vagrant


# Create a throw-away wp-admin user, for doing any pre-pulling setup work that
# we can't do automatically yet.
#
# This should happen last in provisioning, so that the operator can get the
# password for this wp-admin account without scrolling.

echo "Creating a TEMPORARY 'temp_user' wp-admin account..."

# Demote from super-user first, before recycling it.
if [[ $_wp_is_multisite == "true" ]]; then
	if wp --allow-root super-admin list | grep temp_user; then
		echo 'Demoting existing temporary super-admin user...'
	fi
fi

# Recycle the "temp_user" user.
if wp --allow-root user list | grep temp_user; then
	echo 'Deleting existing temporary admin user...'
	wp --allow-root user delete temp_user --yes
fi
wp --allow-root user create temp_user temp@example.com --role=administrator

# Promote to super-user, if applicable.
if [[ $_wp_is_multisite == "true" ]]; then
	echo "Promoting 'temp_user' to super-admin..."
	wp --allow-root super-admin add temp_user
fi

echo
echo " -> You can now log into '$LOCAL_URL/wp-admin' with"
echo "    username - temp_user"
echo "    password - <see above>"
echo

