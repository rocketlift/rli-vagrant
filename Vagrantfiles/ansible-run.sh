#!/usr/bin/env bash

cd /vagrant/Vagrantfiles/ansible

ansible-playbook vagrant.yml \
	-i ./hosts
