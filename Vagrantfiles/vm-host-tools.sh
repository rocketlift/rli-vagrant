#!/usr/bin/env bash

##
# Check the host machine for required programs
##

echo "Checking for VirtualBox..."
printf "VirtualBox " && VBoxManage --version
echo

echo "Checking for Vagrant..."
vagrant --version
echo

vagrant_plugins=(
	vagrant-cachier
	vagrant-hostmanager
)

for plugin in "${vagrant_plugins[@]}"; do
	echo "Checking for the '$plugin' plugin..."
	if ! vagrant plugin list | grep $plugin; then
		echo "ERROR: no '$plugin' found in Vagrant plugins."
		echo "Run 'vagrant plugin install $plugin' to fix this."
	fi
	echo
done

wordpress_plugins=(
	wp-migrate-db-pro
	wp-migrate-db-pro-cli
	wp-migrate-db-pro-media-files
)

echo "Checking for certain WordPress plugins..."
for plugin in "${wordpress_plugins[@]}"; do
	if [ -e wp-content/plugins/$plugin/$plugin.php ]; then
		echo "Found the '$plugin' plugin."
	else
		echo "ERROR: couldn't find '$plugin/$plugin.php' in 'wp-content/plugins'."
		echo "Make sure this plugin is in your the project's dependencies, and"
		echo "that you're running this script from the project root."
	fi
done

