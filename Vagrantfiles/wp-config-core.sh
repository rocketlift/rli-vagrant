#!/usr/bin/env bash

# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi

cd /vagrant

# Create "wp-config.php"
if [ -f /vagrant/wp-config.php ]; then
	echo "Removing old 'wp-config.php' file..."
	rm /vagrant/wp-config.php
fi


echo "Creating a new 'wp-config.php' file..."

# WP Multisite Boolean
if [[ $WP_MULTISITE == true ]]; then
	_wp_is_multisite="true"

else
	_wp_is_multisite="false"
fi

# WP Multisite Type
if [[ $WP_MULTISITE_TYPE == "subdirectory" ]]; then
	_wp_is_subdomain_install="false"
else
	_wp_is_subdomain_install="true"
fi

# WP Multisite Domain
_wp_domain_current_site="$_local_domain"

# WP Multisite Path
if [ -z ${WP_MULTISITE_PATH+/} ]; then
	_wp_path_current_site="$WP_MULTISITE_PATH"
fi

# WP Multisite "Current Site" IDs
if [ -z ${WP_MULTISITE_SITE_ID+1} ]; then
	_wp_site_id_current_site="$WP_MULTISITE_SITE_ID"
fi
if [ -z ${WP_MULTISITE_BLOG_ID+1} ]; then
	_wp_site_id_current_site="$WP_MULTISITE_BLOG_ID"
fi

# WP Multisite Configuration PHP
if [[ $_wp_is_multisite == "true"  ]]; then
	read -r -d '' _wp_config_php_multisite <<EOF
		define( 'MULTISITE', $_wp_is_multisite );
		define( 'SUBDOMAIN_INSTALL', $_wp_is_subdomain_install );
		define( 'DOMAIN_CURRENT_SITE', '$_wp_domain_current_site' );
		define( 'PATH_CURRENT_SITE', '/' );
		define( 'SITE_ID_CURRENT_SITE', 1 );
		define( 'BLOG_ID_CURRENT_SITE', 1 );

else
	read -r -d '' _wp_config_php_multisite <<-EOF
		# (no multisite configuration set)
EOF
fi


_WPMDB_LICENSE="$WPMDB_LICENSE"

# WP Migrate DB Pro Configuration
read -r -d '' _wp_config_php_wpmdb_pro <<EOF
	define( 'WPMDB_LICENCE', '$_WPMDB_LICENSE' );
EOF

# WP Sunrise Configuration
if [[ $WP_SUNRISE == "true"  ]]; then
	read -r -d '' _wp_config_php_sunrise <<EOF
		define( 'SUNRISE', 'on' );
EOF
else
	read -r -d '' _wp_config_php_sunrise <<EOF
		# (no sunrise configuration set)
EOF
fi

# Create the actual "wp-config.php" file
wp core config \
	--path=/vagrant \
	--url=$LOCAL_URL \
	--allow-root \
	--dbhost=127.0.0.1 \
	--dbname=wordpress \
	--dbuser=wordpress \
	--dbpass=wordpress \
	--extra-php <<PHP

		# WP Migrate DB Pro 
		$_wp_config_php_wpmdb_pro

		# WP Multisite settings (if any)
		$_wp_config_php_multisite

		# Extra local WP-Config-like PHP file.
		if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
			require_once( dirname( __FILE__ ) . '/wp-config-local.php' );
		}

		# Sunrise
		$_wp_config_php_sunrise
PHP


# Populate the database with WordPress things
#
# The 'wp core install' or 'wp core multisite-install' command will
# check if it even needs to run, and will exit sanely if nothing
# needs doing.

if [[ $_wp_is_multisite == "true" ]]; then
	_wp_core_install_cmd="multisite-install"
	if [[ $_wp_is_subdomain_install == "true" ]]; then 
		_wp_core_install_opt_subdomains="--subdomains"
	fi
else
	_wp_core_install_cmd="install"
fi

echo "Checking for a WordPress database schema..."
wp core $_wp_core_install_cmd \
	--path=/vagrant \
	--url=$LOCAL_URL \
	--allow-root \
	$_wp_core_install_opt_subdomains \
	--title="Site Title" \
	--admin_user="temp_user" \
	--admin_email="temp@example.com" \
	--admin_password="$(
		cat /dev/urandom \
			| strings \
			| grep -o '[[:alnum:]]' \
			| head -n 30 \
			| tr -d '\n'; echo
		)" \
	--skip-email

