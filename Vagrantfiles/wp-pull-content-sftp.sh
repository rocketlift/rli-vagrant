#!/usr/bin/env bash

##
# Use a CLI SFTP client to pull all site content from production,
# including media assets.
##


# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi


# Whether or not to pull media assets
if [ -z ${RLI_VAGRANT_WP_PULL_MEDIA+true} ]; then
	echo "'RLI_VAGRANT_WP_PULL_MEDIA' is unset, defaulting to 'true'."
	sftp_pull_media=true
else
	echo "'RLI_VAGRANT_WP_PULL_MEDIA' is set to '$RLI_VAGRANT_WP_PULL_MEDIA'."
	sftp_pull_media=false
fi
echo


# Work from the web root
cd /vagrant


# Set some vars from the environment
sftp_port=$PROD_SFTP_PORT
sftp_host=$PROD_SFTP_HOST
sftp_user=$PROD_SFTP_USER
sftp_pass=$PROD_SFTP_PASS
sftp_media_remote_path=$PROD_SFTP_PATH/wp-content/uploads
sftp_media_local_path=/vagrant/wp-content/uploads
sftp_sql_remote_file=$PROD_SFTP_PATH/$PROD_SFTP_SQL_FILE
sftp_sql_local_file=/vagrant/$PROD_SFTP_PATH/$PROD_SFTP_SQL_FILE


# Remove existing local ".sql" dumpfile if it exists
if [ -e "$sftp_sql_local_file" ]; then
	rm "$sftp_sql_local_file"
fi

# This is an unfortunate thing we've got to do, otherwise a new VM
# won't be able to connect automatically, since it doesn't know the
# target key. If "~/.ssh/known_hosts" exists, we can mostly assume
# that it's got cached fingerprints that should be trusted, so we
# should NOT make a "-o StrictHostKeyChecking=no" connection.
echo "Caching SSH fingerprint for '$sftp_host'..."
if [ -e "~/.ssh/known_hosts" ]; then
	echo "File '~/.ssh/known_hosts' exists. Trusting those fingerprints."
else
	sshpass -p $sftp_pass \
		ssh -q \
			-p $sftp_port \
			-o StrictHostKeyChecking=no \
			$sftp_user@$sftp_host
fi
echo

echo "Downloading & importing '$sftp_sql_remote_file'..."
lftp -e "get $sftp_sql_remote_file -o $sftp_sql_local_file; quit" \
	-p $sftp_port \
	-u $sftp_user,$sftp_pass \
	sftp://$sftp_host
wp db import --allow-root $sftp_sql_local_file
echo


# Download media assets
if [[ $sftp_pull_media == "true" ]]; then
	echo "Downloading media assets..."
	lftp -e "mirror --parallel=10 --only-newer $sftp_media_remote_path $sftp_media_local_path; quit" \
		-p $sftp_port \
		-u $sftp_user,$sftp_pass \
		sftp://$sftp_host
	echo
fi


# Clean up
rm "$sftp_sql_local_file"


# Do search-replaces
echo "Running 'wp-search-replaces.sh'..."
echo
/vagrant/Vagrantfiles/wp-search-replaces.sh

# Rebuild ".htaccess" by making WordPress "flush permalinks"
echo "Regenerating permalinks..."
wp --allow-root rewrite flush

