#!/usr/bin/env bash

##
# Use WP Migrate DB Pro CLI to pull all site content from production,
# including media assets.
##

# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi

# Whether or not to pull media assets
if [ -z ${RLI_VAGRANT_WP_PULL_MEDIA+true} ]; then
	echo "'RLI_VAGRANT_WP_PULL_MEDIA' is unset, defaulting to 'true'."
	wpmdb_pull_media=true
else
	echo "'RLI_VAGRANT_WP_PULL_MEDIA' is set to '$RLI_VAGRANT_WP_PULL_MEDIA'."
	wpmdb_pull_media=$RLI_VAGRANT_WP_PULL_MEDIA
fi

# Set media-pulling option
if [[ $wpmdb_pull_media == "true" ]]; then
	wpmdb_pull_media_opt="--media=compare"
fi

# Set excluded post types option
if [ -z ${WPMDB_EXCLUDED_POST_TYPES} ]; then
	echo "'WPMDB_EXCLUDED_POST_TYPES' is not set."
else
	wpmdb_excluded_post_types_opt="--exclude-post-types='$WPMDB_EXCLUDED_POST_TYPES'"
	echo "'WPMDB_EXCLUDED_POST_TYPES' is set to '$WPMDB_EXCLUDED_POST_TYPES'."
fi

# Change to directory where our code is mounted.
cd /vagrant

# Activate WP Migrate DB Pro plugins, so we can do a content-pull
wpmdb_bits=(pro-cli pro-media-files multisite-tools pro)

for wpmdb_bit in "${wpmdb_bits[@]}"; do
	wpmdb_plugin="wp-migrate-db-$wpmdb_bit"
	echo "Probing '$wpmdb_plugin' plugin..."
	if wp plugin status $wpmdb_plugin --path=/vagrant --allow-root; then
		wp plugin activate $wpmdb_plugin \
			--path=/vagrant \
			--allow-root
	else
		echo "Couldn't find the '$wpmdb_plugin' plugin; skipping it."
	fi
done


# Populate site content from Production
echo "Pulling content from Production..."
echo "Settings: pull media assets is '$wpmdb_pull_media' with option '$wpmdb_pull_media_opt'."
echo "Settings: excluded posts types option is '$wpmdb_excluded_post_types_opt'."

wp migratedb pull \
	$PROD_URL \
	$PROD_WPMDB_PRO_KEY \
	--path=/vagrant \
	--allow-root \
	--find=$PROD_URL \
	--replace=$LOCAL_URL \
	$wpmdb_pull_media_opt \
	$wpmdb_excluded_post_types_opt

echo "Skipping 'wp-search-replaces.sh' script, as WPMDB has that functionality."

# Rebuild ".htaccess" by making WordPress "flush permalinks"
echo "Regenerating permalinks..."
wp --allow-root rewrite flush

