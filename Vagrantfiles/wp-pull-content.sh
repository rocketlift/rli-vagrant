#!/usr/bin/env bash

##
# Pull all site content from production, including media assets.
##


# Source the parent repo's environment from the host
if [ -f /vagrant/.env ]; then
	source /vagrant/.env
else
	echo "ERROR: no '.env' file found."
	exit 1
fi


if [[ $RLI_VAGRANT_WP_PULL_CONTENT != "true" ]]; then
	echo "Skipping a content-pull here."
	echo "Set 'RLI_VAGRANT_WP_PULL_CONTENT' from '$RLI_VAGRANT_WP_PULL_CONTENT' to 'true' to change this."
	echo
	echo "Or, run one of the 'wp-pull-content-*.sh' scripts manually."
	echo
	exit 0
fi


# Set pulling method from environment variable
if [ -z ${RLI_VAGRANT_WP_PULL_CONTENT_METHOD}+wpmdb ]; then
	echo "'RLI_VAGRANT_WP_PULL_CONTENT_METHOD' is unset, defaulting to 'wpmdb'."
	wp_pull_method="wpmdb"
else
	echo "'RLI_VAGRANT_WP_PULL_CONTENT_METHOD' is set to '$RLI_VAGRANT_WP_PULL_CONTENT_METHOD'."
	wp_pull_method="$RLI_VAGRANT_WP_PULL_CONTENT_METHOD"
fi
echo


# Run the script that matches the set pulling method
case $wp_pull_method in
	wpmdb)
		echo "Running 'wp-pull-content-wpmdb.sh'..."
		echo
		/vagrant/Vagrantfiles/wp-pull-content-wpmdb.sh
		;;
	sftp)
		echo "Running 'wp-pull-content-sftp.sh'..."
		echo
		/vagrant/Vagrantfiles/wp-pull-content-sftp.sh
		;;
	*)
		echo "Unknown pulling method: '$wp_pull_method'"
		;;
esac

