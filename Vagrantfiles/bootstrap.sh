#!/usr/bin/env bash

# Install Ansible
# Copied from https://github.com/wpengine/hgv/blob/master/bin/hgv-init.sh.
echo
echo "Updating APT sources."
echo
sudo apt-get update > /dev/null
sudo apt-get -y install software-properties-common
sudo add-apt-repository -y ppa:ansible/ansible
echo
echo "Installing Ansible."
echo
sudo apt-get update > /dev/null
sudo apt-get -y install ansible
ansible_version=`dpkg -s ansible 2>&1 | grep Version | cut -f2 -d' '`
echo
echo "Ansible installed ($ansible_version)"

