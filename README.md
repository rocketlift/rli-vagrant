# RLI-Vagrant #

A drop-in [Vagrant](https://www.vagrantup.com/) VM for running a local instance of a website.

## System Requirements ##

Run `./Vagrantfiles/vm-host-tools.sh` to check for these:

* The latest [VirtualBox](https://www.virtualbox.org/), a hypervisor (manages guest virtual machines on your local system).
* The latest [Vagrant](https://www.vagrantup.com/), which scripts virtual machine setup.
    * If upgrading, `vagrant plugin update` afterwards.
* The `vagrant-hostmanager` plugin (optional)
    - `vagrant plugin install vagrant-hostmanager` 
    - This will let Vagrant make your project's `$LOCAL_URL` resolve to the VM's IP address.
    - [More info](https://github.com/devopsgroup-io/vagrant-hostmanager)
* The `vagrant-cachier` plugin (optional)
    - `vagrant plugin install vagrant-cachier ` 
    - This will let multiple VMs use a single global cache for things like software packages. If you run more than one instance of this VM, this will save you significant setup time after your first `vagrant up`.
    - [More info](https://github.com/fgrehm/vagrant-cachier)

## Usage ##

We're assuming your site's project has a `Makefile` that has commands convenient for the project. Consult your project's `make help` to see what's available. Ideally, `make vm-help` will show you everything.

Otherwise, [any normal `vagrant <command>`](https://www.vagrantup.com/docs/cli/) will work as you'd expect.

After SSH-ing to the VM (see below), the Apache error log is located in `/var/log/apache2/error.log`.

### SSH into the VM ###

As a Vagrant VM, all normal `vagrant` commands will work. To SSH into guest VM, just run `vagrant ssh` and everything will be taken care of automatically.

If you need to _manually_ SSH into the guest:

 * SSH hostname: _the hostname or any alias set in your project's `.env` file_.
 * SSH username: `vagrant`
 * SSH password: `vagrant`
 * SSH port: `22`

The port number _should_ be predictable and standard, since each VM gets its own dynamic IP addess; there shouldn't be any port collisions. Note that Vagrant can decide to do some custom port-mapping if it detects any port collisions. Look at the output of a `vagrant up` or `vagrant reload` to see if a funny SSH port gets assigned and forwarded to the guest.

### MySQL access on the VM ###

MySQL is not exposed on the guest. However, you can access the `mysql` command-line via SSH. You can _also_ configure a MySQL client to connect to your guest's database server through an SSH tunnel using the info above.

### Manually Run Extra Scripts ###

Any script that's located in the `Vagrantfiles/` directory can be run manually on-demand, and not all of them are run automatically at provisioning time.

For example, `wp-pull-media.sh` will download media assets _in addition to_ database content:

```
vagrant ssh -c "/vagrant/Vagrantfiles/wp-pull-content.sh"
```

Or, you can:

 1. `vagrant ssh`
 2. `cd /vagrant/Vagrantfiles/`
 3. `./wp-pull-media.sh`

For convenience-tasks that you might add to a project-specific script or `Makefile`, follow the `vagrant ssh -c "<CMD>"` example.

### Manage hosts (domains for sites)

- The `vagrant-hostmanager` plugin does this automatically during provisioning; check your terminal output to confirm.
- Do to this manually, use `vagrant ssh -c "sudo ifconfig"` to find the VM's IP address, and update your Hosts file. This will also be done automatically.

## Development of RLI-Vagrant ##

*__Note!__ The remainder of this document is for developers adding RLI-Vagrant to a project. If you are currently setting up a local instance for a project that already includes `RLI-Vagrant`, everything in the remainder of this document should already be covered by your project's documentation and/or automatic setup tools. Consult with someone familiar with `RLI-Vagrant` if you're confused.*

- Treat `master` as _always stable_. Make branches for everything, and use pull requests liberally.
- Prefix branches by type, i.e. `bugfix/foo` & `feature/bar`.


## Project Requirements for including RLI-Vagrant ##

* A `.env` file defining various environment variables. See the "configuration" section for a list of variables.

* No `wp-config.php` file present. The VM will try to generate its own version of this file so as to make database connections and other things work. If this file already exists, the VM will skip that part of its setup process, and the local site won't be able to talk to its database.

## Installation ##

### Include this code a project ###

For example, in a `peru.yaml` file:

```
imports:
  rli-vagrant: ./
```
```
git module rli-vagrant:
  url: git@bitbucket.org:rocketlift/rli-vagrant.git
  move:
    README.md: docs/modules/RLI-Vagrant.md
  pick:
    - Vagrantfiles/
    - Vagrantfile
    - docs/modules/RLI-Vagrant.md
  recursive: true
```

This module puts the Vagrant code in the parent project's root (`./`) and the documentation (this file) in the parent project's `docs/` folder.

### Ignore the included code ###

Make sure that this repos files are in the site's...

* `.gitignore`. You don't want to be version-controlling these files when your dependency-manager is doing that for you.
* `.buildignore`. You _definitely_ don't want these files getting shipped with your site's builds.

### Add some convenience commands ###

If your project has a `Makefile`, consider adding tasks like the following:

```
vm-up:
	vagrant up --provision
	vagrant ssh -c "cd /vagrant && ./Vagrantfiles/wp-pull-content.sh"
```

## Configuration ##

### Settings ###

Set environment variables in your project's `.env` file. 

*__Note!__ The best practice when adding `RLI-Vagrant` to a project is adding settings as needed to the project's `env-sample` file. This way developers setting up their local environments shouldn't need to consult these docs.*

- `LOCAL_URL` - the full "http://domain.tld.dev" URL for your local instance of the site. No trailing `/`.
- `LOCAL_ALIASES` (optional) - a _comma_-separated list of hostname aliases. No protocol prefix, no trailing `/`. See [the `node.hostmanager.aliases` example code](https://github.com/devopsgroup-io/vagrant-hostmanager#usage) for more.
- `PROD_URL` - the full "https://domain.tld" URL of the Production site. No trailing `/`.
- Virtual Machine attribures: - `RLI_VAGRANT_RAM` (optional) - The size of the VM's RAM. If undefined, a reasonably small default value will be used.
- Content-pulling settings:
  - `RLI_VAGRANT_WP_PULL_CONTENT` (optional)
    - Make the provisioning script download content from the live site. Used by the main provisioning script, but not the content-pulling script.
    - Boolean, defaults to `false`.
  - `RLI_VAGRANT_WP_PULL_CONTENT_METHOD` (optional)
    - Tell the media-downloading script how to do its pulling:
      - `wpmdb` (default) - using WP Migrate DB Pro CLI. Settings for this:
        - `WPMDB_LICENCE` - A valid license key for WP Migrate DB Pro.
        - `PROD_WPMDB_PRO_KEY` - The alpha-numeric key from the Production site's WP Migrate DB Pro "connection info".
		- `WPMDB_EXCLUDED_POST_TYPES` - A comma separated list of post-type names (slugs) to exclude. e.g. `"revision,customize_changeset,csp-report"`
      - `sftp` - using SFTP. Settings for this:
        - `PROD_SFTP_HOST` - the SFTP server's URL, including `sftp://`.
        - `PROD_SFTP_PORT` - the port that the SFTP server listens on. Usually this is `22`, but some servers use alternate port numbers.
        - `PROD_SFTP_USER` - your SFTP account's username.
        - `PROD_SFTP_PASS` - your SFTP account's password.
        - `PROD_SFTP_PATH` - the path on the remote server from which to run SFTP commands. Set as `.` to use whatever the path your SFTP user is logged into. 
        - `PROD_SFTP_SQL_FILE` - the path at which the `.sql` dumpfile is on the server, relative to the path set by `PROD_SFTP_PATH`.
  - `RLI_VAGRANT_WP_PULL_MEDIA` (optional)
    - Make the provisioning script download media assets while downloading database content, which requires `RLI_VAGRANT_WP_PULL_CONTENT` to be `true`.
    - Boolean, defaults to `true`.
- For [WordPress Multisite](https://codex.wordpress.org/Create_A_Network):
  - _Warning_: WP Migrate DB Pro does not (yet) support pulling a multisite network. Unless you've got an alternative means of pulling/importing/generating a site's content, you don't want to enable any of this stuff.
  - `WP_MULTISITE` - Whether or not the site is a multisite. Defaults to `false`. Set to `true` to enable any multisite-related functionality whatsoever.
  - `WP_MULTISITE_TYPE` (optional) - The kind of multisite. Defaults to `subdomain`, can also be `subdirectory`.
  - `WP_MULTISITE_PATH` (optional) - The path to the WordPress application. Defaults to `/`.
  - `WP_MULTISITE_SITE_ID` (optional) - The `site_id` of the "main" site in the network. Defaults to `1`.
  - `WP_MULTISITE_BLOG_ID` (optional) - The `blog_id` of the "main" site in the network. Defaults to `1`.
- `WP_SUNRISE` (optional) - Enable WordPress's `SUNRISE` constant. Boolean, defaults to `false`.

### Custom Provisioning Scripts ###

If the following scripts are present in your project...

- `Vagrantfiles/custom-pre-provisioning.sh`
- `Vagrantfiles/custom-post-provisioning.sh`

...they will automatically be run by the main provisioning script.
