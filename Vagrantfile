# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|
  config.vm.box = "geerlingguy/ubuntu1604"
  # config.vm.network :forwarded_port, guest: 80, host: 8888

  vagrant_dir = File.expand_path(File.dirname(__FILE__))
  env_file = File.join(vagrant_dir, "/.env")

  File.readlines(env_file).each do |line|
    values = line.split("=")
    if values[1] != nil
      if values[0] == 'LOCAL_URL'
        ENV[values[0]] = values[1].gsub(/\A"?https?:\/\/(.+?)"?\Z/, '\1')
      else
        ENV[values[0]] = values[1].gsub(/\A"|"\Z/, '').gsub("\n",'')
      end
    end
  end

  # Set hostname and aliases
  guest_hostname = ENV['LOCAL_URL']
  if ENV['LOCAL_ALIASES'] != nil
    guest_aliases = [ ENV['LOCAL_URL'] ] + ENV['LOCAL_ALIASES'].split(",")
  else
    guest_aliases = ENV['LOCAL_URL']
  end

  # Configure VirtualBox
  config.vm.provider :virtualbox do |vb|

    # Amount of RAM
    if ENV['RLI_VAGRANT_RAM'] != nil
      vb.customize ["modifyvm", :id, "--memory", ENV['RLI_VAGRANT_RAM']]
    end

  end

  # Automatically configure the host machine's "/etc/hosts" file
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
      if vm.id
        `VBoxManage guestproperty get #{vm.id} "/VirtualBox/GuestInfo/Net/1/V4/IP"`.split()[1]
      end
    end

    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
	config.hostmanager.hostname = guest_hostname
    config.hostmanager.aliases = guest_aliases
  end

  # Private Network
  config.vm.network "private_network", type: :dhcp

  # Cache reusable assets (like apt-get packages) outside the VM
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
    config.cache.synced_folder_opts = {
      type: :nfs,
      mount_options: ['rw', 'vers=3', 'tcp', 'nolock']
    }
  end

  # To avoid stdin/tty issues
  # From https://github.com/wpengine/hgv/blob/master/Vagrantfile#L123.
  config.vm.provision "fix-no-tty", type: "shell" do |s|
    s.privileged = false
    s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  # Software bootstrapping
  config.vm.provision "software-bootstrapping", type: "shell" do |s|
    s.keep_color = true
    s.path = "Vagrantfiles/bootstrap.sh"
  end

  # Pre-provisioning script
  $pre_provisioning_script = <<SCRIPT
if [ -e /vagrant/Vagrantfiles/custom-pre-provisioning.sh ]; then
	echo "Running custom pre-provisioning script..."
	/vagrant/Vagrantfiles/custom-pre-provisioning.sh
else
	echo "No pre-provisioning script to run."
fi
SCRIPT
  config.vm.provision "pre-provisioning", type: "shell" do |s|
    s.inline = $pre_provisioning_script
  end

  # Ansible playbook
  config.vm.provision "ansible-playbook", type: "shell" do |s|
    s.keep_color = true
	s.path = "Vagrantfiles/ansible-run.sh"
  end

  # Configure WordPress itself
  config.vm.provision "wp-config-core", type: "shell" do |s|
    s.keep_color = true
	s.path = "Vagrantfiles/wp-config-core.sh"
  end

  # Pull in WordPress content
  config.vm.provision "wp-pull-content", type: "shell" do |s|
    s.keep_color = true
	s.path = "Vagrantfiles/wp-pull-content.sh"
  end

  # Post-provisioning script
  $post_provisioning_script = <<SCRIPT
if [ -e /vagrant/Vagrantfiles/custom-post-provisioning.sh ]; then
	echo "Running custom post-provisioning script..."
	/vagrant/Vagrantfiles/custom-post-provisioning.sh
else
	echo "No post-provisioning script to run."
fi
SCRIPT
  config.vm.provision "post-provisioning", type: "shell" do |s|
    s.inline = $post_provisioning_script
  end

  # Network interface info
  $net_info_script = <<SCRIPT
echo "Showing network info..."
echo
sudo ifconfig
echo
echo "If you don't have the 'vagrant-hostmanager' plugin installed, use the above"
echo "information to alias this machine's IP address in your '/etc/hosts' file."
SCRIPT
  config.vm.provision "net-interfaces", type: "shell" do |s|
    s.privileged = false
    s.inline = $net_info_script
  end

  # WordPress user stuff
  config.vm.provision "wp-config-users", type: "shell" do |s|
    s.path = "Vagrantfiles/wp-config-users.sh"
  end

end
